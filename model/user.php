<?php
class User {

    protected $user_id;
    public $user_name;
    protected $user_email;

    public function getUserId(){
        return $this->user_id;
    }

    public function setUserId($user_id){
        $this->user_id = $user_id;
    }

    public function getUserName(){
        return $this->user_name;
    }

    public function setUserName($user_name){
        $this->user_name = $user_name;
    }
    
    public function getUserEmail(){
        return $this->user_email;
    }

    public function setUserEmail($user_email){
        $this->user_email = $user_email;
    }

}