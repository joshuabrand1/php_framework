<?php
class TestModel {
    
    protected $testDb;
    protected $database = 'testdb';
    protected $class = 'TestDb';

    public function __construct(){
        
        $this->testDb = Db::connect( $this->database, $this->class, Config::get('servername'), 
            Config::get('username'), Config::get('password'), Config::get('dbport') );
    }
    
    public function getMessage($message_id){
        
        return $this->testDb->fetchMessage($message_id);

    }
}