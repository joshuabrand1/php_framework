<?php
abstract class Db {

	protected $pdo = null;

	protected function __construct(pdo $pdo) {
		$this->pdo = $pdo;
	}
	
	public static function connect( $database, $class, $servername, $username, $password, $dbport) {
		
		try {
	        $dsn = "mysql:dbname={$database};host={$servername}";
	        $pdo = new pdo($dsn, $username, $password);
	        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch (PDOException $exception) {
            throw $exception;
//			throw new Exception( 'DB Class connect method raised an exception.  Check the databse exist and is running.  Here is the exception message: '. $exception );
		}
        // $class is the name of the concrete class, see testDb.php for an example 
		return new $class($pdo);
	}

}