<form action="<?= $this->view_data['form_action_uri'] ?>" method="post">

    <div>
        <label style="float:left;" for="user_email">Email Address:</label>
        <input type="text" name="user_email" value="" />
    </div>
    
    <div>
        <button class="button" type="submit"><?= $this->view_data['form_button_text'] ?></button>
    </div>
</form>