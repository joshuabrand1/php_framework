<h2>Users</h2>
<p><a href="/users/create">Create New User</a></p>
<p><?= $this->view_data['flash'] ?></p>
<table class="users">
  <tr>
    <th colspan=3>Action</th>
    <th>No</th>
    <th>Name</th>
    <th>Email</th>
  </tr>
  <tr>
    <?php foreach ($this->view_data['users'] as $user): ?>
      <tr>
        <td><a class="button" href="<?= "/users/show/{$user->getUserId()}" ?>">Show</a></td>
        <td><a class="button" href="<?= "/users/edit/{$user->getUserId()}" ?>">Edit</a></td>
        <td><a class="button" href="<?= "/users/delete/{$user->getUserId()}" ?>">Delete</a></td>
        <td><?= $user->getUserId() ?></td>
        <td><?= $user->getUserName() ?></td>
        <td><?= $user->getUserEmail() ?></td>
      </tr>
    <?php endforeach ?>
  </tr>
</table>
