<h1>Message</h1>
<p class="callout secondary"><?= $this->view_data['flash'] ?></p>
<table>
  <tr>
    <th>User ID</th>
    <th>Name</th>
    <th>Email</th>
  </tr>
  <tr>
      <td><?= $this->view_data['user_id'] ?></td>
      <td><?= $this->view_data['user_name'] ?></td>
      <td><?= $this->view_data['user_email'] ?></td>
  </tr>
</table>
