<form action="<?= $this->view_data['form_action_uri'] ?>" method="post">

    <div>
        <label for="message_text">Message:</label><br />
        <input type="text" name="message_text" value="<?= $this->view_data['message_text'] ?>" />
    </div>
    
    <div>
        <label for="user_id">User ID:</label><br />
        <input type="text" name="user_id" value="<?= $this->view_data['user_id'] ?>" />
    </div>
    
    <div class="button">
        <button type="submit"><?= $this->view_data['form_button_text'] ?></button>
    </div>
</form>