<h1>Message</h1>
<p class="callout secondary"><?= $this->view_data['flash'] ?></p>
<table>
  <tr>
    <th>No</th>
    <th>Message</th>
    <th>User ID</th>
  </tr>
  <tr>
      <td><?= $this->view_data['message_no'] ?></td>
      <td><?= $this->view_data['message_text'] ?></td>
      <td><?= $this->view_data['user_id'] ?></td>
  </tr>
</table>
