<h2>Messages</h2>
<p><a href="/messages/create">Create New Message</a></p>
<p><?= $this->view_data['flash'] ?></p>
<table class="messages">
  <tr>
    <th colspan=3>Action</th>
    <th>No</th>
    <th>Message</th>
    <th>User ID</th>
  </tr>
  <tr>
    <?php foreach ($this->view_data['messages'] as $message): ?>
      <tr>
        <td><a class="button" href="<?= "/messages/show/{$message->getMessageId()}" ?>">Show</a></td>
        <td><a class="button" href="<?= "/messages/edit/{$message->getMessageId()}" ?>">Edit</a></td>
        <td><a class="button" href="<?= "/messages/delete/{$message->getMessageId()}" ?>">Delete</a></td>
        <td><?= $message->getMessageId() ?></td>
        <td><?= $message->getMessageText() ?></td>
        <td><?= $message->getUserID() ?></td>
      </tr>
    <?php endforeach ?>
  </tr>
</table>
