<p><?= $this->view_data['error_message'] ?></p>

<p>An error ocurred on line <?= $this->view_data['exception_getLine'] ?> in file <?= $this->view_data['exception_getFile'] ?></p>

<p>The error message was: <br /><?= $this->view_data['exception_message'] ?></p>

<p>The trace of the sequence of execution before the error: <br /><?= $this->view_data['exception_getTraceAsString'] ?></p>

<p>You will find me in views/errors/show_debug.php</p>
