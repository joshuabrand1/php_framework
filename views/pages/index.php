<h2>Build an MVC framework</h2>
<p>Agile cycles to build an MVC framework based on Convention over Configuration.</p>
<ol>
    <li>App is a front controller that instantiates apllication controllers based on the request URI</li>
    <li>App constructor declares a static router to deal with the request URI</li>
    <li>Router deconstructs friendly URLs: /controller/action/id/params, for example, /messages/show/3 to controller, action, id, and parameters</li>
    <li>Apllication Controller has the <stron>view_data</stron> array for storing data from its respective model to make available in the view with getter and setter methods.</li>
    <li>App then calls the render method on the View object passing it the <strong>view_data</strong> and <strong>view_path</strong>. The partial view for the particular action is populated with the data for the action.</li>
    <li>App creates a second view object passing to it the partial view_data and the layout pathname.</li>
    <li>The view render method finally buffers the output and is returend as the response to the URI request.</li>
</ol>

<p>Note the use of Message Class as a Data Transfer Object.  The PDO parameters are set to return objects rather than associative arrays. </p>
<p>Note the absence of the common singleton connection for the DB as bad practice.  See: Singletons are bad! <a href="https://www.youtube.com/watch?v=-FRm3VPhseI"> GoogleTechTalks - The Clean Code Talks - "Global State and Singletons"</a></p>

<p>See the class and sequence diagrams below for an overview.  The code has debug blocks with a boolean that you can set to see print messages as you walk through the code starting with index.html.  Also see the sequence and the class diagrams below.</p>

<p>Try a simlar use case to the Message CRUD use case.  Then, more ambitiously, try the following:</p>
<ul>
	<li>Security</li>
	<li>Session Management</li>
	<li>Test Framework</li>
	<li>Internationalisation</li>
</ul>
<h2>Class Diagram</h2>
<img src="/assets/images/class.png" alt="Class Diagram">
<h2>Sequance Diagram</h2>
<p>The data is from the controller and does not include database data access.</p>
<img src="/assets/images/sequence_static.png" alt="Sequance Diagram">


<?php if ($this->view_data['debug']): ?>
   <p>You will find me in views/pages/index.html</p>
<?php endif ?> 

