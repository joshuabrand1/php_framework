CREATE DATABASE IF NOT EXISTS testdb;
USE testdb;

DROP TABLE message;

CREATE TABLE IF NOT EXISTS message (
   message_id INT NOT NULL,
   message_text VARCHAR(200) NOT NULL,
   user_id INT,
   PRIMARY KEY ( message_id ),
   FOREIGN KEY ( user_id );
);

INSERT INTO message (message_text, user_id) VALUES ('Hello :)', 1);

DROP TABLE user;

CREATE TABLE IF NOT EXISTS user (
   user_id INT NOT NULL AUTO_INCREMENT,
   user_name VARCHAR(50) NOT NULL,
   user_email VARCHAR(60) NOT NULL,
   PRIMARY KEY ( user_id )
);

INSERT INTO user (user_name, user_email) VALUES ('Joshua Brand', 'josh@brand.co.im');