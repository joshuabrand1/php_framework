<?php
class View{
    
    protected $view_data;
    protected $view_path;
    
    public function __construct($view_data = array(), $view_path = null){

        Config::set('debug', false);
        if( Config::get('debug') ){
            echo '<br />ss===================';
            echo '<br />view: __construct - $view_data = '; print_r($view_data); echo '<br />';       
            echo '<br />view: __construct - $view_path = '; print_r($view_path); echo '<br />';  
            echo '<br />ss===================<br />';
            Config::set('debug', false);
        }

        $this->view_data = $view_data;
        
        $view_path ? $this->view_path = $view_path : $this->view_path = App::getRouter()->getViewPath();  

        if ( !file_exists($this->view_path) ){
            throw new Exception('View file not found: '.$this->view_path);
        }

        Config::set('debug', false);
        if( Config::get('debug') ){
            echo '<br />ee===================';
            echo '<br />view: __construct - $view_data = '; print_r($view_data); echo '<br />';       
            echo '<br />view: __construct - $this->view_data = '; print_r($this->view_data); echo '<br />';        
            echo '<br />view: __construct - $view_path = '; print_r($view_path); echo '<br />';  
            echo '<br />view: __construct - $this->view_path = '; print_r($this->view_path); echo '<br />';  
            echo '<br />ee===================<br />';
            Config::set('debug', false);
        }
        
    }

    public function render(){

        ob_start();
        include($this->view_path);
        $response = ob_get_clean();
        
        Config::set('debug', false);
        if( Config::get('debug') ){
            echo '<br />===================';
            echo '<br /><strong>view: render</strong> - $this->view_data = '; print_r($this->view_data);        
            echo '<br /><strong>view: render</strong> - $this->view_path = '; print_r($this->view_path);  
            echo '<br />===================<br />';
            Config::set('debug', false);
        }
         
        return $response;
        
    }
}