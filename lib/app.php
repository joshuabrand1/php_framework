<?php
class App{

    protected static $router;
    
    public static function run($uri){

        //TODO: refactor to request / dispath
        
        self::$router = new Router($uri);
        
        $controller_class_name = self::$router->getControllerClassName();
        $controller_method = self::$router->getActionName();
        $uri_parameter_id = self::$router->getUriParamId();
            
        if( Config::get('debug') ){
            echo '<br />===================';
            echo '<br /><strong>app: run</strong> - $uri = '; print_r($uri);
            echo '<br /><strong>app: run</strong> - $controller_class_name = '; print_r($controller_class_name);
            echo '<br /><strong>app: run</strong> - $controller_method = '; print_r($controller_method);
                echo '<br /><strong>app: run</strong> - $uri_parameter_id = '; print_r($uri_parameter_id);
            echo '<br />===================<br />';
            Config::set('debug', false);
        }
        
        try {
            
            $controller_object = new $controller_class_name();
            if ($uri_parameter_id) {
                $controller_object->setUriParameterId( $uri_parameter_id );
            }
            $controller_object->$controller_method();
            $view_data = $controller_object->getViewData();
            $view_path = self::$router->getViewPath();
            $view_object = new View( $view_data, $view_path );
            
            Config::set( 'debug', false );
            if( Config::get( 'debug' ) ){
                echo '<br />line 43 ===================';
                echo '<br /><strong>app: run</strong> - $view_path = '; print_r($view_path);
                echo '<br /><strong>app: run</strong> - $view_data = '; print_r($view_data);
                echo '<br /><strong>app: run</strong> - $view_object = '; print_r($view_object);
                echo '<br />ii===================<br />';
                Config::set( 'debug', false );
            }
            
        } catch (Exception $exception) {
            
			$error_controller = new ErrorsController();
			
			//	Security: don't display exception error data to users
			if ( Config::get('display_exceptions') ){
			    $error_controller->showForDebug($exception);
                $view_data = $error_controller->getViewData();
                $view_path = self::$router->getViewDebugErrorPath();
			} else {
			    $error_controller->showToUser();
                $view_data = '';
                $view_path = self::$router->getViewErrorPath();
			}

            $view_object = new View( $view_data, $view_path );
            
            Config::set( 'debug', false );
            if( Config::get( 'debug' ) ){
                echo '<br />======================';
                echo '<br /><strong>app: run</strong> - $view_path = '; print_r($view_path);
                echo '<br /><strong>app: run</strong> - $view_data = '; print_r($view_data);
                echo '<br /><strong>app: run</strong> - $view_object = '; print_r($view_object);
                echo '<br />ii===================<br />';
                Config::set( 'debug', false );
            }

        }
        
        $partial_view = $view_object->render();
        $layout_path = self::$router->getLayoutPath();
        $layout_view_object = new View( compact('partial_view'), $layout_path );

        Config::set('debug', false);
        if( Config::get('debug') ){
            echo '<br />ee===================';
            echo '<br />app: run - $layout_view_object = '; print_r($layout_view_object);
            echo '<br />ee===================<br />';
            Config::set('debug', false);
        }
            
        echo $layout_view_object->render();
    
    }

}