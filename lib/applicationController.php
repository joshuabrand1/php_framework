<?php
class ApplicationController {
    
    protected $view_data;

    public function __construct($view_data=array()){
        $this->view_data = $view_data;
        
        $this->view_data['flash'] = '';
        $this->view_data['debug'] = Config::get('debug');
    }

    public function getViewData(){
        return $this->view_data;
    }
    
}