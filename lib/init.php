<?php
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(dirname(__FILE__)));
define('VIEWS_PATH', ROOT.DS.'views'.DS);

require_once(ROOT.DS.'config'.DS.'config.php');

function __autoload($class_name){
    $lib_path = ROOT.DS.'lib'.DS.lcfirst($class_name).'.php';
    $controller_path = ROOT.DS.'controllers'.DS.lcfirst($class_name).'.php';
    $model_path = ROOT.DS.'model'.DS.lcfirst($class_name).'.php';

    if (file_exists($lib_path)){
        require_once($lib_path);
    } elseif ( file_exists($controller_path)) {
        require_once($controller_path);
    } elseif ( file_exists($model_path)) {
        require_once($model_path);
    } 
    else {
        // TODO: refactor to error controller
       throw new Exception('init: failed to load class: ' . $class_name);
    }

}