<?php
class Router{
    protected $uri;
    protected $controller_name;
    protected $action_name;
    protected $uri_param_id;
    protected $uri_params;
    protected $layout_path;

    public function __construct($uri){

        $this->layout_path = VIEWS_PATH.DS.Config::get('default_layout').'.php';
        $this->controller_name = Config::get('default_controller');
        $this->action_name = Config::get('default_action');

        // uri: /controller_name/action_name/param1/param2/...
        $this->uri = urldecode(trim($uri, '/'));
        $uri_parts = explode('?', $this->uri);
        
        $path = $uri_parts[0];
        $path_parts = explode('/', $path);

        Config::set('debug', false);
        if( Config::get('debug') ){
            echo '<br />===================';
            echo '<br />router: construct - $path_parts = '; print_r($path_parts);        
            echo '<br />===================<br />';
            Config::set('debug', false);
        }
        
        // url /messgaes/show/1 ... hmm! internationalise with /en/messages/show/1
        if ( count($path_parts)) {
            
            // for /messgaes/show/1 - controller_name = messgaes
            if ( current($path_parts)) {
                $this->controller_name = strtolower(current($path_parts));
                array_shift($path_parts);
            }
            
            // for /messgaes/show/1 - action_name = show
            if ( current($path_parts)) {
                $this->action_name = strtolower(current($path_parts));
                array_shift($path_parts);
            }
            
            // for /messgaes/show/1 - uri_param_id = 1
            if ( current($path_parts)) {
                $this->uri_param_id = current($path_parts);
                array_shift($path_parts);
            }
            
            // any remaining parameters e.g. /messages/jumble/seed/20/word/antidisestablishmentarinism
            $this->uri_params = $path_parts;

        } 
        
        Config::set('debug', false);
        if( Config::get('debug') ){
            echo '<br />===================';
            echo '<br />router: construct - $path_parts = '; print_r($path_parts);        
            echo '<br />router: construct - $this->controller_name = '; print_r($this->controller_name);        
            echo '<br />router: construct - $this->action_name = '; print_r($this->action_name);  
            echo '<br />router: construct - $this->uri_param_id =  '; print_r($this->uri_param_id);
            echo '<br />router: construct - $this->uri_params = '; print_r($this->uri_params);
            echo '<br />router: construct - $this->layout_path = '; print_r($this->layout_path);
            echo '<br />===================<br />';
            Config::set('debug', false);
        }
    }

    public function getUri() {
        return $this->uri;
    }
    
    public function getControllerName(){
        return $this->controller_name;
    }

    public function getControllerClassName(){
        return ucfirst( $this->controller_name ).'Controller';
    }

    public function getActionName(){
        return $this->action_name;
    }

    public function getUriParamId(){
        return $this->uri_param_id;
    }

    public function getUriParams(){
        return $this->uri_params;
    }
    
    public function getlayoutPath() {
        return $this->layout_path;
    }

    public function getViewPath(){
      return VIEWS_PATH.self::getControllerName().DS.self::getActionName().'.php';
    }

    public function getViewErrorPath(){
      return VIEWS_PATH.'errors'.DS.'show.php';
    }

    public function getViewDebugErrorPath(){
      return VIEWS_PATH.'errors'.DS.'show_debug.php';
    }

    public function getRequestParams() {
        return $_REQUEST;
    }
    
}