<?php 

// refactor to a config file
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

?>

<!doctype html>
<html lang="en">
	<head>
		<title>My web document</title>
		<meta charset="utf-8" />
		<meta name="author" content="Ebbi Shaghouei" />
		<meta name="description" content="PHP MVC Framework Tutorial" />
		<link href="main.css" rel="stylesheet" type="text/css" />
	</head>
	<body>
		<header>
			<h1>Tests</h1>
		</header>
		<section id="main_section">
			<h2>Test MVC directory structure and routes</h2>
			<h3>Try a range of URLs and see the friendly URLs ready to be processed by a controller.</h3>
			<p> your C9 url / <a href="https://php-ebbi-shaghouei.c9users.io/">Click me!</a></p>
			<p> your C9 url /user/edit/id/1 <a href="https://php-ebbi-shaghouei.c9users.io/user/edit/id/1">Click me!</a></p>
			<p> your C9 url /index.php/user/edit/id/1 <a href="https://php-ebbi-shaghouei.c9users.io/index.php/user/edit/id/1">Click me!</a></p>
			<p> your C9 url /controller/action/param1/value1/param2/value2/ <a href="https://php-ebbi-shaghouei.c9users.io/controller/action/param1/value1/param2/value2/">Click me!</a></p>
		</section>
	</body>
</html>