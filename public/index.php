<?php 

// app initialisation with config parameters and autoload core classes
require_once("../lib/init.php");

// Messages for debuging
Config::set('debug', false);
if( Config::get('debug') ){
    echo '<br /> ===================  Hay Ho! here we go!<br />';
    echo 'index: - $_SERVER[REQUEST_URI] = '; print_r($_SERVER['REQUEST_URI']);
    echo '<br />===================<br />';
}
Config::set('debug', false);

App::run($_SERVER['REQUEST_URI']);

?>