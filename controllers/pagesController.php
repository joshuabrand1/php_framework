<?php
class PagesController extends ApplicationController{

    public function __construct(){
        parent::__construct();
    }
    
    public function index(){
        $this->view_data['test_content'] = 'This is content from the pagesController; see the index method in PagesController';
        $this->view_data['mathematician'] = 'Al Khawarasmi';
        $this->view_data['content'] = 'content from pagesController index';
    }
    
    public function show(){
        $this->view_data['content'] = "Show method in PagesController";
    }
    
}