<?php
class ErrorsController extends ApplicationController{

    public function __construct(){
        parent::__construct();
    }

    public function showToUser(){
        $this->view_data['error_message'] = 'Where would life be without exceptions! but they are caught :)';
    }

    public function showForDebug( $exception) {
        $this->view_data['error_message'] = 'Hay Ho! An exception was caught! Keep Calm! :)';
        $this->view_data['exception_message'] = $exception->getMessage();
        $this->view_data['exception_getCode'] = $exception->getCode();
        $this->view_data['exception_getFile'] = $exception->getFile();
        $this->view_data['exception_getLine'] = $exception->getLine();
        $this->view_data['exception_getTraceAsString'] = $exception->getTraceAsString();
        $this->view_data['exception_getFile'] = $exception->getFile();
    }
    
}