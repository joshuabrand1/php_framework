<?php
class MessagesController extends ApplicationController{
    
    protected $message_model;
    protected $database = 'heroku_35cf23d0a132170';
    protected $class = 'MessageDb';
    protected $message_id;

    public function __construct(){
        
        parent::__construct();

        try {
            $this->message_model = new MessageModel( $this->database, $this->class );
        } catch(Exception $exception){
            throw $exception;
        }

    }
    
    public function setUriParameterId( $id ){
        $this->message_id = $id;
    }
    
    public function getMessageId(){
        return $this->message_id;
    }
    
    protected function getMessage(){
        
        // TODO: refactor to define/check route
        
        if (isset($this->message_id)) {
            $message = $this->message_model->getMessage($this->message_id);
            if ( is_object($message)){
                $this->view_data['flash'] = "Message {$this->message_id} was found.";
                $this->view_data['message_no'] = $this->message_id;
                $this->view_data['message_text'] = $message->getMessageText();
                $this->view_data['user_id'] = $message->getUserID();
            } else {
                $this->view_data['flash'] = "Message {$this->message_id} was not found.";
                $this->view_data['message_no'] = '';
                $this->view_data['message_text'] = '';
                $this->view_data['user_id'] = '';
                return null;
            }
        } else {
            $this->view_data['flash'] = "Message {$this->message_id} was not found!";
            $this->view_data['message_no'] = '';
            $this->view_data['message_text'] = '';
            $this->view_data['user_id'] = '';
            return null;
        }        
    }
    
    // /messages/index
    public function index(){
        $this->view_data['messages'] = $this->message_model->getMessages();
    }
    
    // /messages/show/id/1
    public function show(){
        return $this->getMessage();
    }
    
    // /messages/delete/id/1
    public function delete(){
        
        $message_id = $this->getMessageId();
        $result = $this->message_model->deleteMessage($message_id);

        Config::set('debug', false);
        if( Config::get('debug') ){
            echo '<br />===================';
            echo '<br /><strong>view: render</strong> - $this->getUriParamsId() = '; print_r($this->getUriParamsId());        
            echo '<br /><strong>view: render</strong> - $result = '; print_r($result);  
            echo '<br />===================<br />';
            Config::set('debug', false);
        }

        if ($result > 0){
            $this->view_data['flash'] = 'Message deleted';
            $this->view_data['messages'] = $this->message_model->getMessages();
        } else {
            $this->view_data['flash'] = 'Something went wrong with delete!';
        }
    }
    
    // /messages/new
    public function create(){
        $this->view_data['message_text'] = '';
        $this->view_data['user_id'] = '';
        $this->view_data['form_button_text'] = 'Create';
        $this->view_data['form_action_uri'] = '/messages/save/';
    }
    
    // /messages/save/
    public function save(){
        $message = new Message;
        $message->setMessageText( $_REQUEST['message_text'] );
        $message->setUserID( $_REQUEST['user_id'] );
        $result = $this->message_model->saveMessage($message);
        
        $this->view_data['messages'] = $this->message_model->getMessages();
        $result > 0 ? $this->view_data['flash'] = 'Message saved' : $this->view_data['flash'] = 'Something went wrong with save!';
    }
    
    // /messages/edit/id/1
    public function edit(){
        $message = $this->message_model->getMessage($this->message_id);
        $this->view_data['message_text'] = $message->getMessageText();
        $this->view_data['user_id'] = $message->getUserID();
        $this->view_data['form_action_uri'] = '/messages/update/'.$this->message_id;
        $this->view_data['form_button_text'] = 'Update';
    }
    
    // /messages/update/id/1
    public function update(){
        $requestMessage = array('message_id' => $this->message_id,
            'message_text' => $_REQUEST['message_text'],
            'user_id' => $_REQUEST['user_id']);
        $result = $this->message_model->updateMessage($requestMessage);

        $this->view_data['messages'] = $this->message_model->getMessages();

        $result > 0 ? $this->view_data['flash'] = 'Message updated' : 
            $this->view_data['flash'] = 'Something went wrong with update!';
            
    }
    
}