<?php

error_reporting(E_ALL);
Config::set('debug', false);
Config::set('display_exceptions', false);

Config::set('site_name', 'Chubkins play with PHP!');
Config::set('site_description', 'Building an MVC framework in PHP');

Config::set('default_controller', 'pages');
Config::set('default_action', 'index');
Config::set('default_layout', 'layout');

// DB connection 
Config::set('servername', getenv('IP'));
Config::set('username', getenv('C9_USER'));
Config::set('password', '');
Config::set('dbport', 3306);


    // A simple PHP script demonstrating how to connect to MySQL.
    // Press the 'Run' button on the top to start the web server,
    // then click the URL that is emitted to the Output tab of the console.

/*
    $servername = getenv('IP');
    $username = getenv('C9_USER');
    $password = "";
    $database = "c9";
    $dbport = 3306;

    // Create connection
    $db = new mysqli($servername, $username, $password, $database, $dbport);
    
    */

    // Check connection
    
    


$url = parse_url(getenv("CLEARDB_DATABASE_URL"));

$server = $url["host"];
$username = $url["user"];
$password = $url["pass"];
$database = substr($url["path"], 1);
$dbport = 3306;

$db = new mysqli($server, $username, $password, $database, $dbport);

if ($db->connect_error) {
        die("Connection failed: " . $db->connect_error);
    } 
    echo "Connected successfully (".$db->host_info.")";